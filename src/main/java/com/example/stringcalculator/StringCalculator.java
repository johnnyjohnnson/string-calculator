package com.example.stringcalculator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class StringCalculator {

    int add(String numbers) {

        if (numbers.length() == 0) {
            return 0;
        }

        String[] splittedNumber = numbers.split("[,\\n\\*%]+");
        List<Integer> negNumbers = new ArrayList<>();
        int sum = Stream.of(splittedNumber)
                .map( strElem -> {
                    int i = Integer.valueOf(strElem);
                    if (i < 0) {
                        negNumbers.add(i);
                    } else if (i > 1000) {
                        i = 0;
                    }
                    return i;
                })
                .reduce(0, (elem1, elem2) -> elem1 + elem2);
        if ( negNumbers.size() > 0) {
            throw new NegativesNotAllowedException(negNumbers);
        }
        return sum;
     }
}
