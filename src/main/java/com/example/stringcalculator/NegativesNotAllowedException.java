package com.example.stringcalculator;

import java.util.List;

public class NegativesNotAllowedException extends RuntimeException {

    public NegativesNotAllowedException(List<Integer> negNumbers) {
        super(negNumbers.toString() + " are not allowed");
    }
}
