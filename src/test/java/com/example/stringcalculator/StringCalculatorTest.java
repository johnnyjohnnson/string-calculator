package com.example.stringcalculator;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class StringCalculatorTest {

    StringCalculator stringCalc;

    @BeforeEach
    void setUp() {
        stringCalc = new StringCalculator();
    }

    // if empty String return 0
    @Test
    void add_emptyString_ret0() throws Exception {
        assertEquals(0, stringCalc.add(""));
    }

    // if String contains only one number
    @Test
    void add_singleDigitString_retInput() throws Exception {
        assertEquals(888, stringCalc.add("888"));
    }

    // if String contains two comma-separated numbers
    @Test
    void add_twoDigitString_retSumOfDigits() throws Exception {
        assertEquals(276, stringCalc.add("231,45"));
    }

    // if String contains more than two comma-separated numbers
    @Test
    void add_fourDigitString_retSumOfDigits() throws Exception {
        assertEquals(278, stringCalc.add("231,45,1,1"));
    }

    // values can be separated by newline-characters too
    @Test
    void add_fourDigitsSepByNewlines_retSumOfDigit() throws Exception {
        assertEquals(278, stringCalc.add("231\n45,1,1"));
    }

    // one or more negative Values throw an Exception and get printed out
    @Test
    void add_atLeastOneNegNumber_retException() {
        NegativesNotAllowedException negExc = assertThrows(
                NegativesNotAllowedException.class, () -> stringCalc.add("-1,2,-3,4,-5"));
        assertEquals("[-1, -3, -5] are not allowed", negExc.getMessage());
    }

    // input-values greater than 1000 should be counted as 0
    @Test
    void add_twoNumbersGreaterThan0_ret0() {
        assertEquals(2, stringCalc.add("2,1001"));
    }

    // allow multiple delimiters
    @Test
    void add_multipleDelimInArow_returnSumOfDigit() {
        assertEquals(6, stringCalc.add("1***2\n%*3"));
    }
}
